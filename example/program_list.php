<?php
require '../bootstrap.php';
require 'kint/Kint.class.php';

$programApi = new \Portal\ProgramApi($apiClient);
$programs = $programApi->getPrograms();
?>

<?php include 'includes/header.html'; ?>

<div class="row-fluid">
    <div class="span12 well">
        <h1>Programs:</h1>
        <?php s( $programs ); ?>
    </div>
</div>

<?php include 'includes/footer.html'; ?>