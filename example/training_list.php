<?php
require '../bootstrap.php';

require 'kint/Kint.class.php';

if (!empty($_GET['submitted'])) {
    $trainingApi = new \Portal\TrainingApi($apiClient);
    $trainings = $trainingApi->getTrainings($_GET);

}
include 'includes/header.html';
?>
<div class="row-fluid">
    <div class="span12 well">
        <form class="form-inline" method="get">
                <label for="zip" class="required">
                    Zip Code:
                </label>
                    <input type="text" id="zip" name="zipCode" class="input-small"/>
                <label for="zip" class="required">
                    Program ID:
                </label>
                    <input type="text" id="programId" name="programId" class="input-mini"/>
                    <input class="btn btn-primary" type="submit" name="submitted" value="Search Trainings">
        </form>
    </div>
</div>
<?php if (!empty($trainings)): ?>
<div class="row-fluid">
    <div class="span12 well">
        <h1>Trainings:</h1>
        <?php s($trainings); ?>

    </div>
</div>
<?php endif; ?>
<?php include 'includes/footer.html'; ?>