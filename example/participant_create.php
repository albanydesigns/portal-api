<?php
session_start();

require '../bootstrap.php';

$participant = new \Portal\Model\Participant();

$participantApi = new \Portal\ParticipantApi($apiClient);
$relationshipOptions = $participantApi->getRelationshipOptions();
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $participantApi = new \Portal\ParticipantApi($apiClient);

    var_dump($_POST['registration']);
    $jsonMapper = new JsonMapper();
    $jsonMapper->map($_POST['registration'], $participant);

    print_r($participant);
    die();
    $participant = $participantApi->create($_POST['workshop_id'], $participant);
}

include 'includes/header.html';
?>
<div class="row-fluid">
    <div class="span6 offset3 well">
        <h1>Create Participant</h1>

        <?php if ($participant->id !== null): ?>
            <div class="alert alert-success">
                Account has been created!
            </div>
        <?php endif; ?>

        <?php if ($participant->hasGlobalErrors()): ?>
            <div class="alert alert-error">
                <ul>
                    <li><?php echo join("</li><li>", $participant->getGlobalErrors()); ?></li>
                </ul>
            </div>
        <?php endif; ?>

        <form class="form-horizontal" method="post">
            <div class="control-group">
                <label for="registration_workshop" class=" control-label required">
                    Workshop ID:
                    <span>*</span>
                </label>
                <div class="controls">
                    <input type="text" id="registration_workshop" name="workshop_id" required="required" class="input-mini"/>
                </div>
            </div>

            <div class="control-group">
            <label for="registration_first_name" class=" control-label required">
                First Name
                <span>*</span></label>
                <div class="controls">
                    <input type="text" id="registration_first_name" name="registration[first_name]" value="<?php echo $participant->first_name; ?>" />
                    <?php if ($participant->hasErrors('first_name')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->getErrors('first_name')); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="registration_last_name" class=" control-label required">
                    Last Name
                    <span>*</span></label>
                <div class="controls">
                    <input type="text" id="registration_last_name" name="registration[last_name]" required="required" value="<?php echo $participant->last_name; ?>">
                    <?php if ($participant->hasErrors('last_name')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->getErrors('last_name')); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="registration_phone_number" class=" control-label required">
                    Phone Number
                    <span>*</span></label>
                <div class="controls"><input type="text" id="registration_phone_number" name="registration[phone_number]" required="required" value="<?php echo $participant->phone_number; ?>">
                    <?php if ($participant->hasErrors('phone_number')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->getErrors('phone_number')); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="registration_address_address" class=" control-label required">
                    Street Address
                    <span>*</span></label>
                <div class="controls"><input type="text" id="registration_address_address" name="registration[address][address]" value="<?php echo $participant->address->address; ?>">
                    <?php if ($participant->address !== null && $participant->address->hasErrors('address')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->address->getErrors('address')); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="registration_address_address_cont" class=" control-label optional">
                    Street Address Line 2
                </label>
                <div class="controls"><input type="text" id="registration_address_address_cont" name="registration[address][address_cont]" value="<?php echo $participant->address->address_cont; ?>">
                    <?php if ($participant->address !== null && $participant->address->hasErrors('address_cont')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->address->getErrors('address_cont')); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="registration_address_city" class=" control-label required">
                    City
                    <span>*</span></label>
                <div class="controls"><input type="text" id="registration_address_city" name="registration[address][city]" value="<?php echo $participant->address->city; ?>">
                    <?php if ($participant->address !== null && $participant->address->hasErrors('city')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->address->getErrors('city')); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="registration_address_state" class=" control-label required">
                    State
                    <span>*</span></label>
                <div class="controls">
                    <select id="registration_address_state" name="registration[address][state]" class="input-medium">
                        <option value="NY">New York</option>
                        <option disabled="disabled">-------------------</option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="DC">District of Columbia</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="VI">Virgin Islands</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                    </select>
                    <?php if ($participant->address !== null && $participant->address->hasErrors('state')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->address->getErrors('state')); ?></span>
                    <?php endif; ?>

                </div>
            </div>
            <div class="control-group">
                <label for="registration_address_zip" class=" control-label required">
                    Zip Code
                    <span>*</span></label>
                <div class="controls"><input type="text" id="registration_address_zip" name="registration[address][zip]" class="input-small" value="<?php echo $participant->address->zip; ?>">
                    <?php if ($participant->address !== null && $participant->address->hasErrors('zip')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->address->getErrors('zip')); ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="registration_email" class=" control-label optional">
                    E-mail Address
                </label>
                <div class="controls"><input type="email" id="registration_email" name="registration[email]" value="<?php echo $participant->email; ?>">
                    <?php if ($participant->hasErrors('email')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->getErrors('email')); ?></span>
                    <?php endif; ?>
                </div>
            </div>

            <div class="control-group">
                <label class=" control-label required">
                    I am registering for this workshop for myself
                    <span>*</span></label>
                <div class="controls"><label class="radio"><input type="radio" id="registration_self_register_0" name="registration[self_register]" required="required" value="true">
                        Yes
                    </label><label class="radio"><input type="radio" id="registration_self_register_1" name="registration[self_register]" required="required" value="false">
                        No
                    </label>
                    <?php if ($participant->hasErrors('self_register')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->getErrors('self_register')); ?></span>
                    <?php endif; ?>

                </div>
            </div>

            <div class="control-group">
                <label class=" control-label optional">
                    If registering for someone else, please indicate your relationship
                </label>
                <div class="controls">
                    <select name="registration[relationship]">
                        <?php foreach($relationshipOptions as $option): ?>
                            <option value="<?php echo $option->id; ?>"><?php echo $option->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php if ($participant->hasErrors('relationship')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->getErrors('relationship')); ?></span>
                    <?php endif; ?>

                </div>
            </div>

            <div class="control-group" style="display: none;">
                <label for="registration_relationship_input" class=" control-label optional">
                    Please Specify Organization/Agency/Other
                </label>
                <div class="controls"><input type="text" id="registration_relationship_input" name="registration[relationship_input]">
                    <?php if ($participant->hasErrors('relationship_input')): ?>
                        <span class="help-block alert alert-error"><?php echo join('<br />', $participant->getErrors('relationship_input')); ?></span>
                    <?php endif; ?>
                </div>
            </div>

            <div class="control-group">
                <label class=" control-label required">
                    I would like to receive a confirmation e-mail for this workshop.
                    <span>*</span></label>
                    <div class="controls"><label class="radio"><input type="radio" id="registration_send_email_0" name="registration[send_email]" required="required" value="1">
                        Yes
                    </label><label class="radio"><input type="radio" id="registration_send_email_1" name="registration[send_email]" required="required" value="0">
                        No
                    </label>
                        <?php if ($participant->hasErrors('send_email')): ?>
                            <span class="help-block alert alert-error"><?php echo join('<br />', $participant->getErrors('send_email')); ?></span>
                        <?php endif; ?>
                    </div>
            </div>

            <div class="form-actions">
                <input class="btn btn-primary btn-large" type="submit" name="register" value="Register">
            </div>
        </form>


    </div>
</div>
<?php include 'includes/footer.html'; ?>