<?php
require '../bootstrap.php';

require 'kint/Kint.class.php';

if (!empty($_GET['submitted'])) {
    $workshopApi = new \Portal\WorkshopApi($apiClient);
    $workshops = $workshopApi->getWorkshops($_GET);
}
include 'includes/header.html';
?>

<div class="row-fluid">
    <div class="span12 well">
        <form class="form-inline" method="get">
                <label for="zip" class="required">
                    Zip Code:
                </label>
                    <input type="text" id="zip" name="zipCode" class="input-small"/>
                <label for="zip" class="required">
                    Program ID:
                </label>
                <input type="text" id="programId" name="programId" class="input-mini"/>
                <input class="btn btn-primary" type="submit" name="submitted" value="Search Workshops">
        </form>
    </div>
</div>
<?php if (!empty($workshops)): ?>
<div class="row-fluid">
    <div class="span12 well">
        <h1>Workshops:</h1>
        <?php s($workshops); ?>
        <?php
        //loop example:
        /*
        foreach($workshops as $workshop) {
            echo "ID: " . $workshop->id . "<br>";
            echo "Name: " . $workshop->name . "<br>";
            echo "Program: " . $workshop->program->name . "<br>";
            echo "Schedule: " . "<br>";
            foreach($workshop->schedule as $session) {
                echo $session->start_datetime->format('F j, Y g:iA T') . "<br>";
            }
        }
        */
        ?>
    </div>
</div>
<?php endif; ?>
<?php include 'includes/footer.html'; ?>