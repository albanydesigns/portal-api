<?php
require '../bootstrap.php';

require 'kint/Kint.class.php';

if (!empty($_GET['workshop_id'])) {
    $workshopApi = new \Portal\WorkshopApi($apiClient);
    $workshop = $workshopApi->getWorkshop($_GET['workshop_id']);
}


include 'includes/header.html';
?>
<div class="row-fluid">
    <div class="span12 well">
        <form class="form-horizontal" method="get">
                <label for="registration_workshop" class="required">
                    Workshop ID:
                    <span>*</span>
                </label>
                    <input type="text" id="registration_workshop" name="workshop_id" required="required" class="input-small"/>
                    <input class="btn btn-primary" type="submit" value="View Workshop">
        </form>
    </div>
</div>
<?php if (!empty($workshop)): ?>
<div class="row-fluid">
    <div class="span12 well">
        <h1>Workshop Details: <?php echo $workshop->id; ?></h1>
        <?php s($workshop); ?>
    </div>
</div>
<?php endif; ?>
<?php include 'includes/footer.html'; ?>