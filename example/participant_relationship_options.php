<?php
session_start();

require '../bootstrap.php';

require 'kint/Kint.class.php';
$participantApi = new \Portal\ParticipantApi($apiClient);
$relationshipOptions = $participantApi->getRelationshipOptions();

ob_start();
var_dump($relationshipOptions);
$printOptions = ob_get_clean();
$printOptions = str_replace("=>\n", " => ", $printOptions);

?>

<?php include 'includes/header.html'; ?>

<div class="row-fluid">
    <div class="span12 well">
        <h1>Participant Relationship Options:</h1>
        <?php s( $relationshipOptions ); ?>
    </div>
</div>

<?php include 'includes/footer.html'; ?>