<?php
require '../bootstrap.php';

$programApi = new \Portal\ProgramApi($apiClient);
$programs = $programApi->getPrograms();

$interestRequest = new \Portal\Model\WorkshopInterestRequest();

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $workshopApi = new \Portal\WorkshopApi($apiClient);

    $jsonMapper = new JsonMapper();
    $jsonMapper->map($_POST['workshop_interest_request'], $interestRequest);

    $interestRequest = $workshopApi->createInterestRequest($interestRequest);
}
?>
<?php include 'includes/header.html'; ?>

<div class="row-fluid">
    <div class="span6 offset3 well">
    <h1>Create Interest Request</h1>
    <p>Interested in a workshop that might not be available? Fill out this form and we'll contact you ASAP!</p>
    <hr />
    <?php if ($interestRequest->id !== null): ?>
        <div class="alert alert-success">
            Interest Request Submitted!
        </div>
    <?php endif; ?>

    <?php if ($interestRequest->hasGlobalErrors()): ?>
        <div class="alert alert-error">
            <ul>
                <li><?php echo join("</li><li>", $interestRequest->getGlobalErrors()); ?></li>
            </ul>
        </div>
    <?php endif; ?>

    <form class="form-horizontal" method="post">

    <div class="control-group">
        <label for="workshop_interest_request_first_name" class=" control-label required">
            First Name<span>*</span>
        </label>
        <div class="controls">
            <input type="text" id="workshop_interest_request_first_name" name="workshop_interest_request[first_name]" value="<?php echo $interestRequest->first_name; ?>" />
            <?php if ($interestRequest->hasErrors('first_name')): ?>
                <span class="help-block alert alert-error"><?php echo join('<br />', $interestRequest->getErrors('first_name')); ?></span>
            <?php endif; ?>
        </div>
    </div>
    <div class="control-group">
        <label for="workshop_interest_request_last_name" class=" control-label required">
            Last Name<span>*</span>
        </label>
        <div class="controls">
            <input type="text" id="workshop_interest_request_last_name" name="workshop_interest_request[last_name]" required="required" value="<?php echo $interestRequest->last_name; ?>">
            <?php if ($interestRequest->hasErrors('last_name')): ?>
                <span class="help-block alert alert-error"><?php echo join('<br />', $interestRequest->getErrors('last_name')); ?></span>
            <?php endif; ?>
        </div>
    </div>
    <div class="control-group">
        <label for="workshop_interest_request_phone_number" class=" control-label required">
            Phone Number
            <span>*</span>
        </label>
        <div class="controls"><input type="text" id="workshop_interest_request_phone_number" name="workshop_interest_request[phone_number]" required="required" value="<?php echo $interestRequest->phone_number; ?>">
            <?php if ($interestRequest->hasErrors('phone_number')): ?>
                <span class="help-block alert alert-error"><?php echo join('<br />', $interestRequest->getErrors('phone_number')); ?></span>
            <?php endif; ?>
        </div>
    </div>
    <div class="control-group">
        <label for="workshop_interest_request_email" class=" control-label optional">
            E-mail Address
        </label>
        <div class="controls"><input type="email" id="workshop_interest_request_email" name="workshop_interest_request[email]" value="<?php echo $interestRequest->email; ?>">
            <?php if ($interestRequest->hasErrors('email')): ?>
                <span class="help-block alert alert-error"><?php echo join('<br />', $interestRequest->getErrors('email')); ?></span>
            <?php endif; ?>
        </div>
    </div>
    <div class="control-group">
        <label for="workshop_interest_request_preferred_contact_method" class=" control-label required">
            Preferred Contact Method:<span>*</span>
        </label>
        <div class="controls">
            <select id="workshop_interest_request_preferred_contact_method" name="workshop_interest_request[preferred_contact_method]" required="required" class="input-medium">
                <option value="">Please Select:</option>
                <option value="phone">Phone</option>
                <option value="email">E-mail</option>
            </select>
            <?php if ($interestRequest->hasErrors('preferred_contact_method')): ?>
                <span class="help-block alert alert-error"><?php echo join('<br />', $interestRequest->getErrors('preferred_contact_method')); ?></span>
            <?php endif; ?>
        </div>
    </div>
    <div class="control-group">
        <label for="workshop_interest_request_program" class=" control-label required">
            Program of Interest:<span>*</span>
        </label>
        <div class="controls">
            <select id="workshop_interest_request_program" name="workshop_interest_request[program]" required="required" class="input-medium">
                <option value="">Please Select:</option>
                <?php foreach($programs as $program): ?>
                    <option value="<?php echo $program->id; ?>"><?php echo $program->name; ?></option>
                <?php endforeach; ?>
            </select>
            <?php if ($interestRequest->hasErrors('program')): ?>
                <span class="help-block alert alert-error"><?php echo join('<br />', $interestRequest->getErrors('program')); ?></span>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-actions">
        <input class="btn btn-primary btn-large" type="submit" name="register" value="Register">
    </div>
    </form>


    </div>
    </div>
<?php include 'includes/footer.html'; ?>