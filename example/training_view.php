<?php
require '../bootstrap.php';

require 'kint/Kint.class.php';

if (!empty($_GET['training_id'])) {
    $trainingApi = new \Portal\TrainingApi($apiClient);

    $workshop = $trainingApi->getTraining($_GET['training_id']);
}


include 'includes/header.html';
?>
<div class="row-fluid">
    <div class="span12 well">
        <form class="form-horizontal" method="get">
            <label for="registration_training" class="required">
                Training ID:
                <span>*</span>
            </label>
            <input type="text" id="registration_training" name="training_id" required="required" class="input-small"/>
            <input class="btn btn-primary" type="submit" value="View Training">
        </form>
    </div>
</div>
<?php if (!empty($workshop)): ?>
<div class="row-fluid">
    <div class="span12 well">
        <h1>Training Details: <?php echo $workshop->id; ?></h1>
        <?php s($workshop); ?>
    </div>
</div>
<?php endif; ?>
<?php include 'includes/footer.html'; ?>