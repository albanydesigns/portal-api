<?php

class ParticipantApiTest extends \PHPUnit\Framework\TestCase
{
    public function setUp() 
    {
        $this->mockApiClient = $this->getMockBuilder('Portal\PortalAPIClient')
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockResponse = $this->getMockBuilder('Httpful\Response')
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testGetRelationshipOptions()
    {
        $this->mockResponse->code = 200;
        $this->mockResponse->body = $this->getRelationshipOptions();

        $this->mockApiClient->method('getJson')
            ->willReturn($this->mockResponse);


        $participantApi = new \Portal\ParticipantApi($this->mockApiClient);

        $relationshipOptions = $participantApi->getRelationshipOptions();

        $this->assertInstanceOf('ArrayObject', $relationshipOptions);

        $this->assertEquals(2, $relationshipOptions[1]->id);
        $this->assertEquals(true, $relationshipOptions[1]->require_input);
    }

    public function testCreate()
    {
        $this->mockResponse->code = 200;
        $this->mockResponse->body = (object) array(
            'id' => 1
        );

        $this->mockApiClient->method('post')
            ->willReturn($this->mockResponse);

        $participantApi = new \Portal\ParticipantApi($this->mockApiClient);

        $participant = $participantApi->create(1, $this->getCleanParticipant());

        $this->assertEquals(1, $participant->id);

    }

    public function testCreateInvalid()
    {
        $this->mockResponse->code = 400;
        $this->mockResponse->body = (object) array(
            'errors' => array(
                'children' => array(
                    'first_name' => array(
                        'errors' => array('Invalid')
                    )

                )
            )
        );

        $this->mockApiClient->method('post')
            ->willReturn($this->mockResponse);

        $participantApi = new \Portal\ParticipantApi($this->mockApiClient);

        $participant = $this->getCleanParticipant();
        $participant->first_name = null;

        $participant = $participantApi->create(1, $participant);

        $this->assertTrue($participant->hasErrors('first_name'));

    }

    public function testCreateInvalidAddress()
    {
        $this->mockResponse->code = 400;
        $this->mockResponse->body = (object) array(
            'errors' => array(
                'children' => array(
                    'address' => array(
                        'children' => array(
                            'address' => array(
                                'errors' => array('Invalid')
                            )
                        )
                    )

                )
            )
        );

        $this->mockApiClient->method('post')
            ->willReturn($this->mockResponse);

        $participantApi = new \Portal\ParticipantApi($this->mockApiClient);

        $participant = $this->getCleanParticipant();
        $participant->first_name = null;

        $participant = $participantApi->create(1, $participant);

        $this->assertTrue($participant->address->hasErrors('address'));

    }

    public function testCreateWorkshopNotFound()
    {
        $this->mockResponse->code = 400;
        $this->mockResponse->body = json_decode(json_encode((object) array(
            'code' => 400,
            'message' => 'workshop.notfound'
        )), FALSE);

        $this->mockApiClient->method('post')
            ->willReturn($this->mockResponse);

        $participantApi = new \Portal\ParticipantApi($this->mockApiClient);

        $participant = $participantApi->create(1, $this->getCleanParticipant());

        $this->assertCount(1, $participant->getGlobalErrors());

    }

    public function testCreateWorkshopNoRegistration()
    {
        $this->mockResponse->code = 400;
        $this->mockResponse->body = json_decode(json_encode((object) array(
            'code' => 400,
            'message' => 'workshop.noregistration'
        )), FALSE);

        $this->mockApiClient->method('post')
            ->willReturn($this->mockResponse);

        $participantApi = new \Portal\ParticipantApi($this->mockApiClient);

        $participant = $participantApi->create(1, $this->getCleanParticipant());

        $this->assertCount(1, $participant->getGlobalErrors());

    }

    public function testGetRegistrationFields()
    {
        $participantApi = new \Portal\ParticipantApi($this->mockApiClient);

        $expected = array(
            'first_name' => "Test",
            'last_name' => "Participant",
            'phone_number' => "5185555555",
            'email' => "test@participant.com",
            'self_register' => true,
            'address' => array(
                'address' => "123 Main Street",
                'address_cont' => "Street 2",
                'city' => "Nowhere",
                'state' => "NY",
                'zip' => "10001"
            ),
            'send_email' => true,
            'relationship' => 1,
            'relationship_input' => "Nothing"
        );

        $method = self::getMethod('getRegistrationFields');

        $this->assertEquals($expected, $method->invokeArgs($participantApi, array($this->getCleanParticipant())));
    }

    /**
     * Retreive a private method
     *
     * @param $name
     * @return ReflectionMethod
     */
    protected static function getMethod($name)
    {
        $class = new ReflectionClass('Portal\ParticipantApi');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }

    private function getCleanParticipant()
    {
        $participant = new \Portal\Model\Participant();
        $participant->first_name = "Test";
        $participant->last_name = "Participant";
        $participant->phone_number = "5185555555";
        $participant->email = "test@participant.com";
        $participant->self_register = true;
        $participant->address->address = "123 Main Street";
        $participant->address->address_cont = "Street 2";
        $participant->address->city = "Nowhere";
        $participant->address->state = "NY";
        $participant->address->zip = "10001";
        $participant->send_email = true;
        $participant->relationship = 1;
        $participant->relationship_input = "Nothing";

        return $participant;
    }

    private function getRelationshipOptions()
    {
        return array(
            (object) array(
                'id' => 1,
                'name' => "Option 1",
                'require_input' => false,
                'input_label' => "",
                'position' => 1
            ),
            (object) array(
                'id' => 2,
                'name' => "Option 2",
                'require_input' => true,
                'input_label' => "Test",
                'position' => 2
            )
        );
    }
} 