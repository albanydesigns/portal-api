<?php

use PHPUnit\Framework\TestCase;

class WorkshopApiTest extends TestCase
{
    public function setUp()
    {
        $this->mockApiClient = $this->getMockBuilder('Portal\PortalAPIClient')
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockResponse = $this->getMockBuilder('Httpful\Response')
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testGetWorkshop()
    {
        $this->mockResponse->code = 200;
        $this->mockResponse->body = json_decode($this->getSingleWorkshopJson());

        $this->mockApiClient->method('getJson')
            ->willReturn($this->mockResponse);

        $workshopApi = new \Portal\WorkshopApi($this->mockApiClient);

        $workshop = $workshopApi->getWorkshop(111);

        $this->assertInstanceOf('Portal\Model\Workshop', $workshop);

        $this->assertEquals("Chronic Pain Self-Management Program (CPSMP) - 111-0111", $workshop->name);
        $this->assertEquals("Chronic Pain Self-Management Program (CPSMP)", $workshop->program->name);
        $this->assertEquals("Random Implementation Site", $workshop->implementation_site->name);
        $this->assertEquals("123 Main Street", $workshop->implementation_site->address->address);
        $this->assertEquals("English", $workshop->language->name);
        $this->assertEquals("Health Care Organization", $workshop->implementation_site->delivery_type->name);
        $this->assertCount(6, $workshop->schedule);

    }

    public function testWorkshopInterestRequest()
    {
        $this->mockResponse->code = 200;
        $this->mockResponse->body = (object) array(
            'id' => 1
        );

        $this->mockApiClient->method('post')
            ->willReturn($this->mockResponse);

        $mapper = new \JsonMapper();
        $interestRequest = $mapper->map(json_decode($this->getCleanWorkshopInterestRequestJson()), new \Portal\Model\WorkshopInterestRequest());

        $workshopApi = new \Portal\WorkshopApi($this->mockApiClient);

        $interestRequest = $workshopApi->createInterestRequest($interestRequest);

        $this->assertNotNull($interestRequest->id);

    }

    public function testInvalidWorkshopInterestRequest()
    {
        $this->mockResponse->code = 400;
        $this->mockResponse->body = (object) array(
            'errors' => array(
                'children' => array(
                    'first_name' => array(
                        'errors' => array('Invalid')
                    )

                )
            )
        );

        $this->mockApiClient->method('post')
            ->willReturn($this->mockResponse);

        $mapper = new \JsonMapper();
        $interestRequest = $mapper->map(json_decode($this->getCleanWorkshopInterestRequestJson()), new \Portal\Model\WorkshopInterestRequest());

        $interestRequest->first_name = null;

        $workshopApi = new \Portal\WorkshopApi($this->mockApiClient);

        $status = $workshopApi->createInterestRequest($interestRequest);

        $this->assertInstanceOf(get_class(new \Portal\Model\WorkshopInterestRequest()), $status);
        $this->assertTrue($status->hasErrors('first_name'));

    }

    private function getSingleWorkshopJson()
    {
        return file_get_contents(dirname(__FILE__) . '/data/clean-workshop.json');
    }

    private function getCleanWorkshopInterestRequestJson()
    {
        return file_get_contents(dirname(__FILE__) . '/data/clean-workshop-interest-request.json');
    }

} 