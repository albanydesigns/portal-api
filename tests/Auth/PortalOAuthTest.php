<?php

use PHPUnit\Framework\TestCase;

class PortalOAuthTest extends TestCase
{

    public function testGetUtcTimestamp()
    {
        $oauth = new \Portal\Auth\PortalOAuth(new \Portal\Auth\Storage\OAuthSessionStorage(), "", "", "", "");

        $method = self::getMethod('getUtcTimestamp');

        $this->assertInstanceOf(get_class(new \DateTime()), $method->invokeArgs($oauth, array()));
    }

    public function testGetNonce()
    {
        $oauth = new \Portal\Auth\PortalOAuth(new \Portal\Auth\Storage\OAuthSessionStorage(), "", "", "", "");

        $method = self::getMethod('getNonce');

        $this->assertNotNull($method->invokeArgs($oauth, array()));
    }


    /**
     * Retreive a private method
     *
     * @param $name
     * @return ReflectionMethod
     */
    protected static function getMethod($name)
    {
        $class = new ReflectionClass('Portal\Auth\PortalOAuth');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }
} 