<?php

use PHPUnit\Framework\TestCase;

class ProgramApiTest extends TestCase
{
    public function setUp()
    {
        $this->mockApiClient = $this->getMockBuilder('Portal\PortalAPIClient')
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockResponse = $this->getMockBuilder('Httpful\Response')
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testGetPrograms()
    {
        $this->mockResponse->code = 200;
        $this->mockResponse->body = json_decode($this->getProgramsJson());

        $this->mockApiClient->method('getJson')
            ->willReturn($this->mockResponse);


        $programApi = new \Portal\ProgramApi($this->mockApiClient);

        $programs = $programApi->getPrograms();

        $this->assertInstanceOf('ArrayObject', $programs);

        $program = $programs->offsetGet("1");

        $this->assertNotEmpty($program->name);
        $this->assertNotEmpty($program->short_name);
        $this->assertNotEmpty($program->description);
        $this->assertFalse($program->require_parent_enrollment);
        $this->assertEmpty($program->parent);
        $this->assertEmpty($program->modules);
        $this->assertNotEmpty($program->nb_sessions);

        //assert a child program
        $childProgram = $programs->offsetGet("9");
        $this->assertTrue($childProgram->require_parent_enrollment);
        $this->assertNotEmpty($childProgram->parent);
        $this->assertNotEmpty($childProgram->parent->name);

        //assert program with modules
        $moduleProgram = $programs->offsetGet("13");
        $this->assertNotEmpty($moduleProgram->modules);
        $this->assertNotEmpty($moduleProgram->modules[0]->name);
        $this->assertNotEmpty($moduleProgram->modules[0]->instructions);
        $this->assertNotEmpty($moduleProgram->modules[0]->file_description);
        $this->assertNotNull($moduleProgram->modules[0]->priority);

        $this->assertNotEmpty($moduleProgram->modules[0]->steps[0]->name);
        $this->assertTrue($moduleProgram->modules[0]->steps[0]->registration_step);

    }

    private function getProgramsJson()
    {
        return file_get_contents(dirname(__FILE__) . "/data/clean-programs.json");
    }
} 