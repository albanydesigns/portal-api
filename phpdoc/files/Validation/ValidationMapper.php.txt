<?php
namespace Portal\Validation;

class ValidationMapper
{

    public static function validate(ValidationEntity $entity, array $response)
    {
        $entity->clearErrors();
        if (isset($response)) {
            if (isset($response['errors'])) {
                foreach($response['errors'] as $error) {
                    $entity->addGlobalError($error);
                }
            }

            if (isset($response['children'])) {
                foreach($response['children'] as $field => $child) {

                    if (isset($child['children']) && isset($entity->$field) && $entity->$field instanceof ValidationEntity) {
                        $entity->$field = self::validate($entity->$field, $child);

                    } elseif (isset($child['errors'])) {

                        foreach($child['errors'] as $error) {
                            $entity->addError($field, $error);
                        }

                    }
                }
            }
        }

        return $entity;
    }
} 
