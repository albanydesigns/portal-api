# API Installation Instructions

## Install source: If already using composer:

1.  Add repository to composer.json:
```
"repositories": [
        {
            "url": "https://albanydesigns@bitbucket.org/albanydesigns/portal-api.git",
            "type": "git"
        }
],
```

1. Add API dependency:
```
"albanydesigns/portal-api" : "v1.0.5"
```

## Install source: If not using composer:

Clone the portal API from git and install the required dependencies:

``` shell
git clone https://albanydesigns@bitbucket.org/albanydesigns/portal-api.git portal-api

cd portal-api

curl -sS https://getcomposer.org/installer | php

php composer.phar install --no-dev
```

----

# Using the API

1.  Copy bootstrap.php-dist as bootstrap.php
2.  Update the OAuth parameters in bootstrap.php to your specific portal installation.
3.  Refer to the examples for different usages.  

## TIPS:

1.  By default, OAuth tokens are stored in the session.  It is recommended that you implement your own Storage mechanism to store these values in a database (or the like).  Simply implement your own version of  Portal\Auth\Storage\OAuthTokenStorageInterface, and use your own bootstrap.php to pass the storage mechanism into the PortalOAuth client.
