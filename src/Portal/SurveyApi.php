<?php
namespace Portal;

use Portal\Exception\UnknownErrorException;

class SurveyApi
{
    const URL_GET_SURVEY = "/surveys/get";
    const URL_POST_SURVEY = "/surveys/post";

    /**
     * @param PortalAPIClient $api_client The API Client prepopulated with credentials.
     */
    public function __construct(PortalAPIClient $api_client)
    {
        $this->api_client = $api_client;
    }

    /**
     * You can retrieve Survey HTML forms (wrapped with Bootstrap HTML tags) once a participant has registered.
     *
     * @param $survey_id
     * @param $participant_id
     * @return array|object|string
     * @throws Exception\UnknownErrorException
     */
    public function getSurveyForm($survey_id, $participant_id)
    {
        $params = array('survey_id' => $survey_id, 'participant_id' => $participant_id);

        $resp = $this->api_client->getHtml(self::URL_GET_SURVEY, $params);

        switch($resp->code) {
            case 200:
                return $resp->body;
            default:
                throw new UnknownErrorException($resp);
        }
    }

    /**
     * Post the array of data returned from the HTML form.
     *
     * @param $data
     * @param $survey_id
     * @param $participant_id
     * @return array|bool|object|string returns boolean of true if successful, the body of html if false.
     */
    public function postSurveyForm($data, $survey_id, $participant_id)
    {
        $params = array('survey_id' => $survey_id, 'participant_id' => $participant_id);

        $resp = $this->api_client->post(self::URL_POST_SURVEY . "?" . http_build_query($params), $data);

        switch($resp->code) {
            case 200:
                return true;
            default:
                return $resp->body;
        }
    }
} 