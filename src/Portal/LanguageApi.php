<?php
namespace Portal;

use JsonMapper;
use Portal\Exception\UnknownErrorException;
use Portal\Model\Language;
use Portal\Model\Program;

class LanguageApi
{
    private $api_client;

    const URL_LANGUAGE_LIST = "/languages/list";

    /**
     * @param PortalAPIClient $api_client The API Client prepopulated with credentials.
     */
    public function __construct(PortalAPIClient $api_client)
    {
        $this->api_client = $api_client;
    }

    /**
     * Retrieve a list of Language objects from the application.
     *
     * @return mixed
     * @throws Exception\UnknownErrorException
     */
    public function getLanguages()
    {
        $resp = $this->api_client->getJson(self::URL_LANGUAGE_LIST);

        switch($resp->code) {
            case 200:
                $mapper = new JsonMapper();
                return $mapper->mapArray($resp->body, new \ArrayObject(), get_class(new Language()));
            default:
                throw new UnknownErrorException($resp);
        }
    }
} 