<?php
namespace Portal;

use Httpful\Http;
use Httpful\Request;
use JsonMapper;
use Portal\Auth\AuthInterface;

class PortalAPIClient
{
    /**
     * @var string
     */
    private $api_url;

    /**
     * @var string
     */
    private $api_version;

    /**
     * @var string
     */
    private $api_path = "/api";

    /**
     * @var Auth\AuthInterface
     */
    private $authManager;

    /**
     * @param $api_url      The hostname of the portal (IE https://portal.ceacw.org)
     * @param $private_key  Private Key (SECRET!)
     * @param $public_key   Public Key
     * @param string $api_version API Version (default 1.0)
     */
    function __construct(AuthInterface $authManager, $api_url, $api_version = "1.0")
    {
        $this->api_url = $api_url . $this->api_path . "/json/" . $api_version;
        $this->api_version = $api_version;

        $this->authManager = $authManager;
        Request::ini($this->getTemplate());
    }

    /**
     * Retrieve data in JSON format.
     *
     * @param $uri
     * @param $params
     * @return \Httpful\Response
     */
    public function getJson($uri, $params = array())
    {
        $query_string = http_build_query(array_merge($params, $this->authManager->getApiParams()));

        return Request::get($this->api_url . $uri . "?" . $query_string)
            ->expectsJson()
            ->withoutStrictSSL()
            ->send();
    }

    /**
     * Retrieve data in HTML format.
     *
     * @param $uri
     * @param $params
     * @return \Httpful\Response
     */
    public function getHtml($uri, $params = array())
    {
        $query_string = http_build_query(array_merge($params, $this->authManager->getApiParams()));

        return Request::get($this->api_url . $uri . "?" . $query_string)
            ->expectsHtml()
            ->withoutStrictSSL()
            ->send();
    }

    /**
     * POST data to the server.
     *
     * @param $uri
     * @param array $data
     * @return \Httpful\Response
     */
    public function post($uri, array $data)
    {
        $url = $this->api_url . $uri;
        $query = parse_url($url, PHP_URL_QUERY);

        if ($query) {
            $url .= "&" . http_build_query($this->authManager->getApiParams());
        } else {
            $url .= "?" . http_build_query($this->authManager->getApiParams());
        }

        return Request::post($url)
            ->sendsJson()
            ->withoutStrictSSL()
            ->body(json_encode($data))
            ->send();
    }

    /**
     * Build a request template that is used for all requests.
     *
     * @param $uri
     * @param $method
     * @return Request
     */
    private function getTemplate()
    {
        return Request::init();
    }

    /**
     * Helper method.
     *
     * @param $data
     * @param $object
     * @return object
     */
    public function dataToObject($data, $object)
    {
        $mapper = new JsonMapper();
        return $mapper->map($data, $object);
    }

    /**
     * Helper method.
     *
     * @param $data
     * @param $object_class
     * @return mixed
     */
    public function dataToObjectArray($data, $object_class)
    {
        $mapper = new JsonMapper();
        return $mapper->mapArray($data, new \ArrayObject(), $object_class);
    }



} 