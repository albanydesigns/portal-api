<?php
namespace Portal\Validation;

class ValidationEntity
{
    private $field_errors = array();

    private $global_errors = array();

    public function addError($field, $error)
    {
        $this->field_errors[$field][] = $error;
    }

    public function getFieldErrors()
    {
        return $this->field_errors;
    }

    public function getErrors($field)
    {
        return isset($this->field_errors[$field]) ? $this->field_errors[$field] : null;
    }

    public function hasErrors($field = null)
    {
        if ($field == null) {
            if (!empty($this->field_errors) || !empty($this->global_errors)) {
                return true;
            }
        }
        return isset($this->field_errors[$field]);
    }

    public function addGlobalError($error)
    {
        $this->global_errors[] = $error;
    }

    public function hasGlobalErrors()
    {
        return count($this->global_errors) > 0;
    }

    public function getGlobalErrors()
    {
        return $this->global_errors;
    }

    public function clearErrors()
    {
        $this->global_errors = array();
        $this->field_errors = array();
    }
} 