<?php
namespace Portal;

use JsonMapper;
use Portal\Exception\UnknownErrorException;
use Portal\Model\Location\Address;
use Portal\Model\Participant;
use Portal\Model\RelationshipOption;
use Portal\Validation\ValidationMapper;

class ParticipantApi
{
    private $api_client;

    const URL_CREATE = "/participant/create";
    const URL_CONSENT = "/participant/consent";
    const URL_RELATIONSHIP_OPTIONS = "/participant/relationship-options";

    public function __construct(PortalAPIClient $api_client)
    {
        $this->api_client = $api_client;
    }

    public function getRelationshipOptions()
    {
        $resp = $this->api_client->getJson(self::URL_RELATIONSHIP_OPTIONS);

        switch($resp->code) {
            case 200:
                $mapper = new JsonMapper();
                return $mapper->mapArray($resp->body, new \ArrayObject(), get_class(new RelationshipOption()));
            default:
                throw new UnknownErrorException($resp);
        }
    }

    /**
     * Sign the electronic signature for a participant.
     *
     * @param $workshop_id
     * @param Participant $participant
     * @return object|Participant|Validation\ValidationEntity
     */
    public function consent(Participant $participant)
    {

        $resp = $this->api_client->post(self::URL_CONSENT,
                                        array('participant_id' => $participant->id,
                                              'participant_consent' =>
                                                array('electronic_signature' => $participant->electronic_signature)
                                        )
        );

        switch($resp->code) {
            case 200:
                $participant->clearErrors();
                $mapper = new JsonMapper();
                return $mapper->map($resp->body, $participant);
            default:
                $response_array = json_decode(json_encode($resp->body), true);
                return ValidationMapper::validate($participant, $response_array['errors']);
        }
    }

    /**
     * Create a new participant.
     *
     * @param $workshop_id
     * @param Participant $participant
     * @return object|Participant|Validation\ValidationEntity
     */
    public function create($workshop_id, Participant $participant)
    {
        $params = array('workshop_id' => $workshop_id) + array('registration' => $this->getRegistrationFields($participant));

        $resp = $this->api_client->post(self::URL_CREATE . "?" . http_build_query($params), $params);

        switch($resp->code) {
            case 200:
                $participant->clearErrors();
                $mapper = new JsonMapper();
                return $mapper->map($resp->body, $participant);
            case 400:
                $response_array = json_decode(json_encode($resp->body), true);

                if (isset($response_array['message'])) {
                    switch($response_array['message']) {
                        case "workshop.noregistration":
                            $participant->addGlobalError("This workshop is currently not accepting new participants.");
                            return $participant;
                        case "workshop.notfound":
                            $participant->addGlobalError("Workshop could not be located.");
                            return $participant;
                        case "Validation Failed":
                            return ValidationMapper::validate($participant, $response_array['errors']);
                         default: //validation error
                             throw new UnknownErrorException($response_array['message']);
                    }
                } else {
                    return ValidationMapper::validate($participant, $response_array['errors']);
                }

            default:
                throw new UnknownErrorException($resp->body->message);
        }

    }

    /**
     * Return array of fields used for registration.
     *
     * @param Participant $participant
     * @return array
     */
    private function getRegistrationFields(Participant $participant)
    {
        $data = array();
        $data['first_name'] = $participant->first_name;
        $data['last_name'] = $participant->last_name;
        $data['email'] = $participant->email;
        $data['phone_number'] = $participant->phone_number;
        $data['self_register'] = $participant->self_register;
        $data['relationship'] = $participant->relationship;
        $data['relationship_input'] = $participant->relationship_input;
        $data['send_email'] = $participant->send_email;

        if ($participant->address instanceof Address) {
            $data['address']['address'] = $participant->address->address;
            $data['address']['address_cont'] = $participant->address->address_cont;
            $data['address']['city'] = $participant->address->city;
            $data['address']['state'] = $participant->address->state;
            $data['address']['zip'] = $participant->address->zip;
        }

        return $data;
    }
} 