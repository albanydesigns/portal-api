<?php
namespace Portal\Auth;

use Httpful\Request;
use Portal\Auth\Storage\OAuthTokenStorageInterface;

class PortalOAuth implements AuthInterface
{
    private $client_id;

    private $client_secret;

    private $api_secret;

    private $portal_url;

    const OAUTH_TOKEN_PATH = "/oauth/v2/token";
    const OAUTH_GRANT_TYPE = "http://portal.local/grants/api_key";

    /**
     * @var OAuthTokenStorageInterface
     */
    private $token_storage;

    function __construct(OAuthTokenStorageInterface $token_storage, $portal_url, $client_id, $client_secret, $api_secret)
    {
        $this->portal_url = $portal_url;
        $this->api_secret = $api_secret;
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->token_storage = $token_storage;
    }

    /**
     * Build API params once authenticated.  Used in PortalAPIClient.
     *
     * @return array
     */
    public function getApiParams()
    {
        if ($this->token_storage->isExpired() === false) {
            return array('access_token' => $this->token_storage->getAccessToken());
        }

        if ($this->token_storage->getRefreshToken() !== null) {
            $this->refreshToken();
            return $this->getApiParams();
        } else {
            $this->generateTokens();
            return $this->getApiParams();
        }

    }

    public function getApiHeaders()
    {
        return array();
    }

    /**
     * Retrieve another access_token.
     *
     * @return
     */
    public function refreshToken()
    {
        $params = array(
            'refresh_token' => $this->token_storage->getRefreshToken(),
            'grant_type' => 'refresh_token',
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret
        );

        $resp = $this->getJsonResponse($this->portal_url . self::OAUTH_TOKEN_PATH . "?" . http_build_query($params));

        if ($resp->code === 401) {
            $this->generateTokens();
            return;
        }

        $this->token_storage->setAccessToken($resp->body->access_token);
        $this->token_storage->setRefreshToken(($resp->body->refresh_token));
        $timestamp = new \DateTime();
        $timestamp->setTimestamp(time() + $resp->body->expires_in);
        $this->token_storage->setExpiresAt($timestamp);

    }

    /**
     * Generate an initial access token based on client credentials.
     *
     * @throws AuthException
     */
    public function generateTokens()
    {
        $timestamp = $this->getUtcTimestamp();
        $nonce = $this->getNonce();

        $params = array(
            'grant_type' => self::OAUTH_GRANT_TYPE,
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'nonce' => $nonce,
            'created_at' => $timestamp->getTimestamp(),
            'api_key' => sha1($nonce .  $this->api_secret . $timestamp->getTimestamp())
        );

        $resp = $this->getJsonResponse($this->portal_url . self::OAUTH_TOKEN_PATH . "?" . http_build_query($params));

        if ($resp->code !== 200) {
            throw new AuthException($resp->body->error_description);
        }

        $this->token_storage->setAccessToken($resp->body->access_token);
        $this->token_storage->setRefreshToken($resp->body->refresh_token);

        $timestamp = new \DateTime();
        $timestamp->setTimestamp(time() + $resp->body->expires_in);
        $this->token_storage->setExpiresAt($timestamp);

    }

    /**
     * Helper method to call OAuth items.
     *
     * @param $url
     * @return mixed
     */
    private function getJsonResponse($url)
    {
        return Request::get($url)
                    ->withoutStrictSSL()
                    ->expectsJson()
                    ->send();
    }

    /**
     * @return \DateTime UTC Timestamp.
     */
    private function getUtcTimestamp()
    {
        $timestamp = new \DateTime();
        $timestamp->setTimezone(new \DateTimeZone("UTC"));

        return $timestamp;
    }

    /**
     * @param int $length
     * @return string A randomized string of $length characters.
     */
    private function getNonce($length = 20) {

        $chars = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
        $ll = strlen($chars)-1;
        $o = '';

        while (strlen($o) < $length) {
            $o .= $chars[ rand(0, $ll) ];
        }

        return sha1($o);
    }


} 