<?php
namespace Portal\Auth\Storage;

interface OAuthTokenStorageInterface
{

    /**
     * @param $token Access token
     * @return mixed
     */
    public function setAccessToken($token);

    /**
     * @param \DateTime $expirationDate
     * @return mixed
     */
    public function setExpiresAt(\DateTime $expirationDate);

    /**
     * @return boolean
     */
    public function isExpired();

    /**
     * @return mixed The current active access token
     */
    public function getAccessToken();

    /**
     * @param $token The current refresh token
     */
    public function setRefreshToken($token);

    /**
     * @return mixed The current refresh token
     */
    public function getRefreshToken();

} 