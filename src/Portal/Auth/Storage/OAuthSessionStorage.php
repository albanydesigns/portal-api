<?php
namespace Portal\Auth\Storage;

class OAuthSessionStorage implements OAuthTokenStorageInterface
{
    const SESSION_ACCESS_TOKEN = 'portal_oauth_access_token';
    const SESSION_ACCESS_TOKEN_EXPIRE = 'portal_oauth_access_token_expiration';
    const SESSION_REFRESH_TOKEN = 'portal_oauth_refresh_token';

    /**
     * Store the access token.
     *
     * @param $token
     * @return mixed
     */
    public function setAccessToken($token)
    {
        $_SESSION[self::SESSION_ACCESS_TOKEN] = $token;
    }

    public function setExpiresAt(\DateTime $expirationDate)
    {
        $_SESSION[self::SESSION_ACCESS_TOKEN_EXPIRE] = $expirationDate->getTimestamp();
    }

    public function isExpired()
    {
        if ($this->getAccessToken() === null) {
            return true;
        }

        if (empty($_SESSION[self::SESSION_ACCESS_TOKEN_EXPIRE])) {
            return true;
        }

        if (time() > $_SESSION[self::SESSION_ACCESS_TOKEN_EXPIRE]) {
            return true;
        }

        return false;

    }

    /**
     * @return mixed The current active access token
     */
    public function getAccessToken()
    {
        return isset($_SESSION[self::SESSION_ACCESS_TOKEN]) ? $_SESSION[self::SESSION_ACCESS_TOKEN] : null;
    }

    /**
     * @param $token The current refresh token
     */
    public function setRefreshToken($token)
    {
        $_SESSION[self::SESSION_REFRESH_TOKEN] = $token;
    }

    /**
     * @return mixed The current refresh token
     */
    public function getRefreshToken()
    {
        return isset($_SESSION[self::SESSION_REFRESH_TOKEN]) ? $_SESSION[self::SESSION_REFRESH_TOKEN] : null;
    }


} 