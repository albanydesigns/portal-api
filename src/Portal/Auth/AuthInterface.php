<?php
/**
 * Created by PhpStorm.
 * User: MIKE
 * Date: 9/1/14
 * Time: 12:42 PM
 */

namespace Portal\Auth;


interface AuthInterface {

    public function getApiParams();

    public function getApiHeaders();
} 