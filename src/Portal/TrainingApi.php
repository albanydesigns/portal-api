<?php
namespace Portal;

use JsonMapper;
use Portal\Exception\UnknownErrorException;
use Portal\Model\Training\Training;

class TrainingApi
{
    private $api_client;

    const URL_TRAINING_VIEW = "/training/view/[ID]";
    const URL_TRAINING_LIST = "/training/list";

    /**
     * @param PortalAPIClient $api_client The API Client prepopulated with credentials.
     */
    public function __construct(PortalAPIClient $api_client)
    {
        $this->api_client = $api_client;
    }

    /**
     * Retrieve a single training by ID.
     *
     * @param $trainingId
     * @return null|Training
     * @throws \Exception
     */
    public function getTraining($trainingId)
    {
        $resp = $this->api_client->getJson(str_replace('[ID]', $trainingId, self::URL_TRAINING_VIEW));

        switch($resp->code) {
            case 200:
                $mapper = new JsonMapper();
                $data = $resp->body;
                return $mapper->map($data, new Training());
            case 400:
                return null;
            default:
                throw new \Exception($resp->body);
        }

    }

    /**
     * Search for trainings.
     *
     * Params currently accepted are:
     *
     * 'zipCode' : zipCode to search for trainings near
     * 'distance' : distance from zip code (default is 30).
     * 'programId' : ID of a program to narrow trainings by.
     *
     * @param $params
     * @return array|\ArrayObject<Training>
     * @throws \Exception
     */
    public function getTrainings($params = array())
    {
        $resp = $this->api_client->getJson(self::URL_TRAINING_LIST, $params);

        switch($resp->code) {
            case 200:
                $mapper = new JsonMapper();

                /** @var \ArrayObject $arrayObject */
                return $mapper->mapArray($resp->body, new \ArrayObject(), get_class(new Training()));

            case 400:
                return array();
            default:
                throw new UnknownErrorException($resp->code);
        }

    }

} 