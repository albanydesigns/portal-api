<?php
namespace Portal;

use JsonMapper;
use Portal\Exception\UnknownErrorException;
use Portal\Model\Workshop;
use Portal\Model\WorkshopInterestRequest;
use Portal\Validation\ValidationMapper;

class WorkshopApi
{
    private $api_client;

    const URL_WS_VIEW = "/ws/view";
    const URL_WS_VIEW_WWE = "/ws/view-wwe";
    const URL_WS_LIST = "/ws/list";
    const URL_WS_INTEREST_REQUEST = "/ws/interest-request";

    /**
     * @param PortalAPIClient $api_client The API Client prepopulated with credentials.
     */
    public function __construct(PortalAPIClient $api_client)
    {
        $this->api_client = $api_client;
    }

    /**
     * Create a Workshop Interest Request in the Application.
     *
     * @param WorkshopInterestRequest $interestRequest An object holding the required data for an interest request.
     *                                                 Any errors (global or field-specific) will be available in the returned object.
     *
     * @return object|Validation\ValidationEntity If successful, the WorkshopInterestRequest object will be returned with the "id" field populated.
     *                                            Otherwise, the object will be returned with field_errors or global_errors, as described in the ValidationEntity class.
     *
     * @throws Exception\UnknownErrorException
     */
    public function createInterestRequest(WorkshopInterestRequest $interestRequest)
    {
        $data = array(
            'first_name' => $interestRequest->first_name,
            'last_name' => $interestRequest->last_name,
            'preferred_contact_method' => $interestRequest->preferred_contact_method,
            'program_interest' => $interestRequest->program_interest,
            'email' => $interestRequest->email,
            'phone_number' => $interestRequest->phone_number,
            'comments' => $interestRequest->comments,
        );

        if ($interestRequest->address !== null) {
            $data['address']['address'] = $interestRequest->address->address;
            $data['address']['address_cont'] = $interestRequest->address->address_cont;
            $data['address']['city'] = $interestRequest->address->city;
            $data['address']['state'] = $interestRequest->address->state;
            $data['address']['zip'] = $interestRequest->address->zip;
        }

        $params = array('workshop_interest_request' => $data);

        $resp = $this->api_client->post(self::URL_WS_INTEREST_REQUEST, $params);

        switch($resp->code) {
            case 200:
                $mapper = new JsonMapper();

                return $mapper->map($resp->body, $interestRequest);
            case 400:
                $response_array = json_decode(json_encode($resp->body), true);
                return ValidationMapper::validate($interestRequest, $response_array['errors']);
            default:

                throw new UnknownErrorException($resp->body->message);
        }
    }

    /**
     * Retrieve a single workshop by ID.
     *
     * @param integer $workshopId
     * @param boolean $ignoreAllowRegistration If you want to return a workshop even if no registration is alloed, set this to true
     * @return null|Workshop
     * @throws \Exception
     */
    public function getWorkshop($workshopId, $ignoreAllowRegistration = false)
    {
        $resp = $this->api_client->getJson(self::URL_WS_VIEW, array('workshopId' => $workshopId, 'ignoreAllowRegistration' => (bool) $ignoreAllowRegistration));

        switch($resp->code) {
            case 200:
                $mapper = new JsonMapper();
                return $mapper->map($resp->body, new Workshop());
            case 400:
                return null;
            default:
                throw new \Exception($resp->body);
        }
    }

    /**
     * Retrieve the primary WWE SD Workshop.
     *
     * @return null|Workshop
     * @throws \Exception
     */
    public function getWweWorkshop()
    {
        $resp = $this->api_client->getJson(self::URL_WS_VIEW_WWE);

        switch($resp->code) {
            case 200:
                $mapper = new JsonMapper();
                return $mapper->map($resp->body, new Workshop());
            case 400:
                return null;
            default:
                throw new \Exception($resp->body);
        }

    }

    /**
     * Retrieve workshops near a particular zip code.
     *
     * @param $zipCode
     * @return array|\ArrayObject<Workshop>
     * @throws \Exception
     * @deprecated Use getWorkshops($params)
     */
    public function getWorkshopsByZipCode($zipCode)
    {
        $resp = $this->api_client->getJson(self::URL_WS_LIST, array('zipCode' => $zipCode));

        switch($resp->code) {
            case 200:
                $mapper = new JsonMapper();

                /** @var \ArrayObject $arrayObject */
                return $mapper->mapArray($resp->body, new \ArrayObject(), get_class(new Workshop()));

            case 400:
                return array();
            default:
                throw new \Exception($resp->body);
        }

    }

    /**
     * Search for workshops.
     *
     * Params currently accepted are:
     *
     * 'zipCode' : zipCode to search for workshops near
     * 'distance' : distance from zip code (default is 30).
     * 'programId' : ID of a program to narrow workshops by.
     * 'languageId' : ID of a language to narrow workshops by.
     * 'ownerId' : ID of the Owner to search by.
     *
     * @param $params
     * @return array|\ArrayObject<Workshop>
     * @throws \Exception
     */
    public function getWorkshops($params)
    {
        $resp = $this->api_client->getJson(self::URL_WS_LIST, $params);

        switch($resp->code) {
            case 200:
                $mapper = new JsonMapper();

                /** @var \ArrayObject $arrayObject */
                return $mapper->mapArray($resp->body, new \ArrayObject(), get_class(new Workshop()));

            case 400:
                return array();
            default:
                throw new \Exception($resp->body);
        }

    }
} 