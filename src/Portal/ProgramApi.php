<?php
namespace Portal;

use JsonMapper;
use Portal\Exception\UnknownErrorException;
use Portal\Model\Program;

class ProgramApi
{
    private $api_client;

    const URL_PROGRAM_LIST = "/programs/list";

    /**
     * @param PortalAPIClient $api_client The API Client prepopulated with credentials.
     */
    public function __construct(PortalAPIClient $api_client)
    {
        $this->api_client = $api_client;
    }

    /**
     * Retrieve a list of Program objects from the application.
     *
     * @return mixed
     * @throws Exception\UnknownErrorException
     */
    public function getPrograms()
    {
        $resp = $this->api_client->getJson(self::URL_PROGRAM_LIST);

        switch($resp->code) {
            case 200:
                $mapper = new JsonMapper();
                return $mapper->mapArray($resp->body, new \ArrayObject(), get_class(new Program()));
            default:
                throw new UnknownErrorException($resp);
        }
    }
} 