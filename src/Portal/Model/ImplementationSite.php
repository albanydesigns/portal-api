<?php
namespace Portal\Model;

class ImplementationSite 
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var \Portal\Model\Location\Address
     */
    public $address;

    /**
     * @var \Portal\Model\DeliveryType
     */
    public $delivery_type;

} 