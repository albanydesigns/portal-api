<?php
namespace Portal\Model;

class Language 
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;
} 