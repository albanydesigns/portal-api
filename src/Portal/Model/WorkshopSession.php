<?php
namespace Portal\Model;

class WorkshopSession
{
    /**
     * @var \DateTime
     */
    public $start_datetime;

    /**
     * @var \DateTime
     */
    public $end_datetime;

    /**
     * @var boolean
     */
    public $post_core = false;
}
