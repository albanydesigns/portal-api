<?php
namespace Portal\Model;

class Workshop
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $name_short;

    /**
     * @var string
     */
    public $local_program_name;

    /**
     * @var \DateTime
     */
    public $start_datetime;

    /**
     * @var boolean
     */
    public $allow_registration;

    /**
     * @var float
     */
    public $cost;

    /**
     * @var string
     */
    public $extra_description;

    /**
     * @var string
     */
    public $contact_email;

    /**
     * @var string
     */
    public $contact_phone;

    /**
     * @var string
     */
    public $contact_phone_extension;

    /**
     * @var \Portal\Model\Language
     */
    public $language;

    /**
     * @var \Portal\Model\WorkshopSession[]
     */
    public $schedule;

    /**
     * @var \Portal\Model\ImplementationSite The site/location of the workshop
     */
    public $implementation_site;

    /**
     * @var \Portal\Model\Program The program details of the workshop
     */
    public $program;

    /**
     * @var boolean Has this workshop reached the capacity for the program?
     */
    public $is_full;

    /**
     * @var boolean Is this workshop only meant to be displayed for info, and no registration allowed?
     */
    public $public_listing_info_only;

}
