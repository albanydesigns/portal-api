<?php
namespace Portal\Model;

class WorkshopModule
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string Name of the downloadable file attached to this module.
     */
    public $file_description;

    /**
     * @var int ID of the survey assigned to this module.
     */
    public $survey_id;

    /**
     * @var string
     */
    public $survey_name;


    public $child_surveys;

    /**
     * @var string
     */
    public $instructions;

    /**
     * @var \Portal\Model\WorkshopModuleStep[]
     */
    public $steps;

    /**
     * @var int
     */
    public $priority;

    /**
     * @var bool
     */
    public $consent_module;

    /**
     * @var bool
     */
    public $hidden;


}
