<?php
namespace Portal\Model;

class RelationshipOption 
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var bool
     * @required
     */
    public $require_input = false;

    /**
     * @var string
     */
    public $input_label;

    /**
     * @var int
     */
    public $position;
} 