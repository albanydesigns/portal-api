<?php
namespace Portal\Model\Training;

class TrainingSession 
{

    /**
     * @var \DateTime
     */
    public $start_datetime;

    /**
     * @var \Datetime
     */
    public $end_datetime;

    /**
     * @var \Portal\Model\Training\Training
     */
    public $training;

} 