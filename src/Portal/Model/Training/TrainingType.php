<?php
namespace Portal\Model\Training;

class TrainingType 
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;
} 