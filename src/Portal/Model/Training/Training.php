<?php
namespace Portal\Model\Training;

class Training 
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $location_name;

    /**
     * @var \Portal\Model\Location\Address
     */
    public $address;

    /**
     * @var boolean
     */
    public $is_online;

    /**
     * @var \DateTime
     */
    public $start_datetime;

    /**
     * @var \Portal\Model\Training\TrainingSession[]
     */
    public $schedule;

    /**
     * @var string
     */
    public $contact_name;

    /**
     * @var string
     */
    public $contact_email;

    /**
     * @var string
     */
    public $contact_phone;

    /**
     * @var \Portal\Model\Program
     */
    public $program;

    /**
     * @var \Portal\Model\Training\TrainingType
     */
    public $type;

} 