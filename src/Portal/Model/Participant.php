<?php
namespace Portal\Model;


use Portal\Model\Location\Address;
use Portal\Validation\ValidationEntity;

class Participant extends ValidationEntity
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $last_name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone_number;

    /**
     * @var \Portal\Model\Location\Address
     */
    public $address;

    /**
     * @var int
     */
    public $self_register;

    /**
     * @var int
     */
    public $relationship;

    /**
     * @var string
     */
    public $relationship_input;

    /**
     * @var string
     */
    public $referral_method;

    /**
     * @var \Portal\Model\Workshop
     */
    public $workshop;

    /**
     * @var string
     */
    public $electronic_signature;

    /**
     * @var bool
     */
    public $send_email;

    /**
     * @var boolean
     */
    public $waitlist;

    public function __construct() {
        $this->address = new Address();
    }

}