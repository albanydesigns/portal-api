<?php
namespace Portal\Model;

class Program 
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $short_name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var int
     */
    public $nb_sessions;

    /**
     * @var \Portal\Model\WorkshopModule[]
     */
    public $modules;

    /**
     * @var \Portal\Model\Program
     */
    public $parent;

    /**
     * @var boolean
     */
    public $require_parent_enrollment;
} 