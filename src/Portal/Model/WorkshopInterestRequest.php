<?php
namespace Portal\Model;

use Portal\Validation\ValidationEntity;

class WorkshopInterestRequest extends ValidationEntity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $last_name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone_number;

    /**
     * @var string
     */
    public $preferred_contact_method;

    /**
     * @var int
     */
    public $program_interest;

    /**
     * @var \Portal\Model\Location\Address
     */
    public $address;

    /**
     * @var string
     */
    public $comments;
} 