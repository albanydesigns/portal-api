<?php
namespace Portal\Model\Location;

use Portal\Validation\ValidationEntity;

class Address extends ValidationEntity
{
    public $address;
    public $address_cont;
    public $suite;
    public $city;
    public $state;
    public $zip;

    /**
     * @var \Portal\Model\Location\County
     */
    public $county;

    /**
     * @var \Portal\Model\Location\PointLocation
     */
    public $point_location;

    public function toArray()
    {
        $address = array();

        $address[] = $this->address;

        if ($this->address_cont !== null) {
            $address[] = $this->address_cont;
        }

        if ($this->suite !== null) {
            $address[] = $this->suite;
        }

        $address[] = $this->city . ", " . $this->state . " " . $this->zip;

        return $address;
    }

}
