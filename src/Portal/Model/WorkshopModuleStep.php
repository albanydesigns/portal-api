<?php
namespace Portal\Model;

class WorkshopModuleStep
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $days_from_signup;

    /**
     * @var boolean
     */
    public $registration_step;

    /**
     * @var boolean
     */
    public $completion_step;

}